FROM php:7.4-apache

# Request system packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    libpng-dev \
    libjpeg-dev \
    wget \
    git \
    unzip \
    msmtp \
    diffutils \
    msmtp \
    gettext \
    imagemagick

RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && rm -rf /var/lib/apt/lists/*

# PHP extensions
RUN docker-php-ext-install mysqli gd

# PHP configuration
COPY ./docker/php.ini /usr/local/etc/php/php.ini

# Install Composer
RUN wget wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet && \
    mv composer.phar /usr/local/bin/composer
ENV COMPOSER_HOME=/tmp/composer

#  Scripts
COPY ./docker/start.sh /start.sh
COPY ./docker/initialize.sh /initialize.sh
RUN chmod +x /start.sh /initialize.sh

# Get Mediawiki
RUN wget https://releases.wikimedia.org/mediawiki/1.35/mediawiki-1.35.3.tar.gz -O temp.tar.gz && \
    mkdir /build && \
    tar -xzf temp.tar.gz -C /build && \
    mv /build/mediawiki-1.35.3 /app && \
    rm temp.tar.gz && \
    rm -Rf /build

# Get extensions
RUN wget https://extdist.wmflabs.org/dist/extensions/ConfirmAccount-REL1_36-07c6000.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/MsCatSelect-REL1_36-406e599.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/MobileFrontend-REL1_35-f6d5bcc.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/PageForms-REL1_35-c2dc512.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://github.com/DaSchTour/matomo-mediawiki-extension/archive/v4.0.1.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz && \
    mv /app/extensions/matomo-mediawiki-extension-4.0.1 /app/extensions/Matomo

# Get skins
RUN wget https://extdist.wmflabs.org/dist/skins/MinervaNeue-REL1_35-6c99418.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/skins/CologneBlue-REL1_36-e3413d8.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/skins/Modern-REL1_36-92d32d6.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

# Additional configuration
COPY ./docker/000-default.conf /etc/apache2/sites-available
COPY ./docker/msmtp.conf /app/docker/msmtp.conf
COPY ./docker/remoteip.conf /etc/apache2/conf-available/remoteip.conf

# Apache modules
RUN a2enmod rewrite remoteip
RUN a2enconf remoteip

WORKDIR /app

# Create images symlink to storage
RUN rm -Rf /app/images && \
    ln -s /storage/images /app/images

# Install Mediawiki packages
RUN composer require --update-no-dev \
    mediawiki/semantic-media-wiki "3.2.3" \
    mediawiki/maps "7.20.1" \
    mediawiki/image-map "dev-REL1_35"

RUN apt-get update && apt-get remove -y \
    libpng-dev \
    libjpeg-dev \
    linux-libc-dev

COPY ./docker/LocalSettings.php /app
COPY ./assets /app/assets
COPY ./docker/robots.txt /app

# Define run script
CMD ["/start.sh"]
