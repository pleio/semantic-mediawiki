# Semantic Mediawiki
This repository contains the installation for the Pleio Semantic Mediawiki.

## Building the image
Run the following script to build and push the Docker images

    ./build-and-push-docker-images.sh


## Running the image

    docker run -it pleio/semantic-mediawiki \
        -e SITE_NAME=Mediawiki \
        -e SERVER=http://mediawiki.test:8080 \
        -e FROM_EMAIL=noreply@mediawiki.test \
        -e DB_SERVER=127.0.0.1 \
        -e DB_NAME=mediawiki \
        -e DB_USER=mediawiki \
        -e DB_PASSWORD=mediawiki \
        -e SECRET_KEY=12345678 \
        -e UPGRADE_KEY=12345678 \
        -e SEMANTIC_NAMESPACE=http://mediawiki.test:8080 \
        -e SMTP_HOST=127.0.0.1 \
        -e SMTP_HOSTNAME=smtp.mediawiki.test \
        -v /tmp/storage:/storage \
        -p 80:8080
